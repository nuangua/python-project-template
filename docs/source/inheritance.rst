.. PAT documentation master file, created by
    sphinx-quickstart on Wed Oct 17 15:02:48 2018.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.
    BEGIN CCCT SECTION
    # -------------------------------------------------------------------------
    # Copyright (C) 2019 Intel Corporation
    # Copyright (C) 2018 Intel Corporation
    # Copyright (C) 2016-2017 Intel Deutschland GmbH
    # Copyright (C) 2015-2016 Intel Deutschland GmbH
    # Copyright (C) 2012-2016 Intel Mobile Communications GmbH
    #
    # Sec Class: Intel Confidential (IC)
    # ----------------------------------------------------------------------
    # Revision Information:
    # $File name:
    # ----------------------------------------------------------------------
    # by CCCT (0.12b)
    # ----------------------------------------------------------------------
    # END CCCT SECTION



python-project-template Inheritance Diagram
==============================================

.. inheritance-diagram:: python_project_template.config python_project_template.version python_project_template.demo
    :parts: 1
    :top-classes: main