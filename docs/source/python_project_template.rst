python\_project\_template package
=================================

Submodules
----------

.. toctree::

   python_project_template.cli
   python_project_template.config
   python_project_template.demo
   python_project_template.version

Module contents
---------------

.. automodule:: python_project_template
   :members:
   :undoc-members:
   :show-inheritance:
