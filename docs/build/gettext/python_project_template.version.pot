# SOME DESCRIPTIVE TITLE.
# Copyright (C) DataLab Team-Gu, David(nuanguang.gu@intel.com)
# This file is distributed under the same license as the python-project-template package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: python-project-template <module 'python_project_template.version' from '/Users/sunny/anaconda3/envs/py37/lib/python3.7/site-packages/python_project_template/version.py'>\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-16 16:38+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/python_project_template.version.rst:2
msgid "python\\_project\\_template.version module"
msgstr ""

