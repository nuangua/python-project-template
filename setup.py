# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
from python_project_template import version, config
from setuptools import setup, find_packages

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass = {'build_sphinx': BuildDoc}
except ImportError:
    print("Sphinx is not installed! Please use 'pip install -r requirements.txt' to install if you want to build sphinx docs")
    pass


version.update_version()

SCRIPTS = []
for root, dirs, files in os.walk('bin'):
    for file in files:
        file_path = os.path.join(root, file)
        if os.name=='posix' and file.endswith('.sh'):
            SCRIPTS.append(file_path)
        elif os.name=='nt' and file.endswith('.cmd'):
            SCRIPTS.append(file_path)
        elif os.name=='nt' and file.endswith('.bat'):
            SCRIPTS.append(file_path)
        elif os.name=='nt' and file.endswith('.ps'):
            SCRIPTS.append(file_path)

setup(
    name=config.PROJECT_NAME,
    version=version.get_version(),
    author=config.AUTHOR_NAME,
    author_email=config.AUTHOR_EMAIL,
    packages=find_packages(),
    cmdclass=cmdclass,
    command_options={
        'build_sphinx': {
            'project': ('setup.py', config.PROJECT_NAME),
            'version': ('setup.py', version.get_version()),
            'release': ('setup.py', version.get_release()),
            'source_dir': ('setup.py', 'docs/source'),
            'build_dir': ('setup.py', 'public/build'),
            'builder': ('setup.py', 'html'),
        }
    },
    scripts=SCRIPTS,
    url="{0}/{1}".format(config.REPO_ROOT_URL, config.PROJECT_NAME),
    license=open('LICENSE').read() if os.path.exists("LICENSE") else config.COPYRIGHT,
    description=config.PROJECT_DESC,
    long_description=open('README.rst').read() if os.path.exists("README.rst") else config.PROJECT_DESC,
    include_package_data=True,
    data_files=[],
    # Any requirements here
    install_requires=open('requirements.txt').readlines() if os.path.exists("requirements.txt") else [],
    python_requires=">=2.7",
)
