# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
from datetime import datetime

from python_project_template import config

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

#: update version
def update_version():
    today = datetime.now().isocalendar()

    _version_ = "{0}.{1}.{2}".format(today[0] % 100, today[1], today[2])
    stored_version = None
    if os.path.exists(os.path.join(CURRENT_PATH, config.VERSION_FILENAME)):
        stored_version = open(os.path.join(CURRENT_PATH, config.VERSION_FILENAME)).read()
    if stored_version != _version_:
        with open(os.path.join(CURRENT_PATH, config.VERSION_FILENAME), "w") as fh:
            fh.write(_version_)
    return _version_


#: get version
def get_version():
    stored_version = None
    if os.path.exists(os.path.join(CURRENT_PATH, config.VERSION_FILENAME)):
        stored_version = open(os.path.join(CURRENT_PATH, config.VERSION_FILENAME)).read()
    if stored_version == None:
        stored_version = update_version()
    return stored_version


#: update release
def update_release():
    _release_ = None
    try:
        from subprocess import check_output
        _release_ = check_output(['git', 'describe', '--tags', '--always'], stderr="/dev/null")
        _release_ = _release_.decode().strip()
    except Exception:
        if os.path.exists(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)):
            _release_ = open(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)).read()

    stored_release = None
    if os.path.exists(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)):
        stored_release = open(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)).read()
    if stored_release != _release_:
        with open(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME), "w") as fh:
            fh.write(_release_)
    return _release_


#: get release
def get_release():
    stored_release = None
    if os.path.exists(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)):
        stored_release = open(os.path.join(CURRENT_PATH, config.RELEASE_FILENAME)).read()
    if stored_release == None:
        stored_release = update_release()
    return stored_release
