#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# Copyright (C) 2019 Intel Corporation
# Copyright (C) 2018 Intel Corporation
# Copyright (C) 2016-2017 Intel Deutschland GmbH
# Copyright (C) 2015-2016 Intel Deutschland GmbH
# Copyright (C) 2012-2016 Intel Mobile Communications GmbH
# -------------------------------------------------------------------------
# Author: David Gu(nuanguang.gu@intel.com)
# -------------------------------------------------------------------------

from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import *
from python_project_template import __version__
from plumbum import cli
from python_project_template import version

class Prompt(cli.Application):
    """Prompt CLI
    """

    VERSION = version.get_version()

    def main(self, *args):
        if args:
            print("Unknown command {0!r}".format(args[0]))
            return 1
        if not self.nested_command:
            print("No command given")
            return 1

@Prompt.subcommand("sub")
class PromptSub(cli.Application):
    """Sub command
    """

    def main(self, *args):
        print("This is sub command")


if __name__=="__main__":
    Prompt.run()