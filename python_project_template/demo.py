#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# Copyright (C) 2019 Intel Corporation
# Copyright (C) 2018 Intel Corporation
# Copyright (C) 2016-2017 Intel Deutschland GmbH
# Copyright (C) 2015-2016 Intel Deutschland GmbH
# Copyright (C) 2012-2016 Intel Mobile Communications GmbH
# -------------------------------------------------------------------------
# Author: David Gu(nuanguang.gu@intel.com)
# -------------------------------------------------------------------------

from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import *

"""
This is demo module
"""

class Demo(object):
    """This is a demo class
    """

    def func(self):
        """This is function description
        """
        print("Entering func")
        print("Exiting func")

    def func1(self, param):
        """

        :param param:
        :return:
        """
        pass
