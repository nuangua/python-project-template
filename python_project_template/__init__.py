from python_project_template import config, version

__author__ = config.AUTHOR
__version__ = version.get_version()