# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os

#: Project root path
BASE_PATH = os.path.dirname(os.path.abspath(__file__))

#: Version file name
VERSION_FILENAME = "VERSION"

#: Release file name
RELEASE_FILENAME = "RELEASE"

# Configurations for generating Sphinx documentation
REPO_ROOT_URL = "https://gitlab.devtools.intel.com/DataLab"
PROJECT_NAME = "python-project-template"
PROJECT_DESC = "Python Project Template"
AUTHOR_NAME = "Gu, David"
AUTHOR_EMAIL = "nuanguang.gu@intel.com"
AUTHOR = "{0}({1})".format(AUTHOR_NAME, AUTHOR_EMAIL)
TEAM_NAME = "DataLab Team"
COPYRIGHT = "{0} - {1} Copyright Reserved".format(TEAM_NAME, AUTHOR)

# Specify Graphviz2 dot.exe Path
GRAPHVIZ_DOT=r'C:\Program Files (x86)\Graphviz2.38\bin\dot.exe'
