# python-project-template

Python Project Template

## Prerequisites

* Python 2.7 or greater is required
* OS can be Windows, MacOS or Ubuntu

## Development

* Install Dependencies modules

```buildoutcfg
python -m pip install -r requirements.txt
```

* Package Project source codes

Run below command, the project source codes will be packaged into file under path .\dist.

```buildoutcfg
python setup.py sdist
```

* Generate API and User documentation

Before this, you have to install Graphviz and specify the dot.exe file path as GRAPHVIZ_DOT in pat\config.py.

Run below command, the documentation will be generated in the path .\sphinx-doc\build\html.

```buildoutcfg
python setup.py build_sphinx
```

## Installation

Since we have got the package python-project-template-x.x.x.tar.gz, we can now install it and its dependencies modules as below:

```buildoutcfg
pip install dist\python-project-template-x.x.x.tar.gz
```

## API Documentation

[API Documentation](https://nuangua.gitlab.io/python-project-template/build/html/)

## Release Notes

### 19.22.3

### 19.23.2

* Add static websites pages
* Add LICENSE
* Add requirements.txt
* Add scripts files to Python script path during pip install